/**
 * Modul mit Klassen und Funktionen des Klaviers und der Midi-Informationen.
 */


/**
 * Keyboard - Klassenkonstruktor
 *
 * @param  {type} midi_start Midi Startnote
 * @param  {type} midi_end   Midi Endnote
 * @param  {type} container  Container, in welchem das Klavier gezeichnet werden soll.
 */
function Keyboard(midi_start, midi_end, container){
  this.midi_start = midi_start;
  this.midi_end = midi_end;
  this.container = container;
  this.key_pressed_handlers = [];
  this.keys = Midi.Notes(this.midi_start, this.midi_end);
}

/**
 * Keyboard.update - Übernehmen der aktuellen Konfiguration auf die Klaviertastatur.
 *
 * @param  {type} midi_start = null Midi-Startnote
 * @param  {type} midi_end = null   Midi-Endnote
 */
Keyboard.prototype.update = function (midi_start = null, midi_end = null) {
  if (midi_start == null && midi_end == null){
    console.log("Y bother me?");
  } else if (midi_start == null && midi_end != null){
    midi_start = this.midi_start;
  } else if (midi_start != null && midi_end == null) {
    midi_end = this.midi_end;
  }
  this.midi_start = midi_start;
  this.midi_end = midi_end;
  this.keys = Midi.Notes(this.midi_start, this.midi_end);
  this.render();
};

/**
 * Keyboard.render - Zeichne die Klaviertastatur.
 *
 * Ausmaße hängen ab vom zur Verfügung stehenden Viewport und Notenreichweite.
 *
 */
Keyboard.prototype.render = function () {
  // Neue Ausmaße bestimmen
  var container_width = this.container.width();
  var white_keys = this.keys.filter((elem, i, array) => {return elem.sign == "";});
  var num_white_keys = white_keys.length;
  var key_dimensions = Keys.CalcDimensions(container_width, num_white_keys);
  var keyboard_width = num_white_keys * key_dimensions.white.width;
  var rest = container_width - keyboard_width;
  var left_offset = rest / 2;
  // Aktuelle Ansicht leeren
  this.container.empty();
  // Neue Ansicht zeichnen
  var keyboard_template = `<div></div>`;
  var $keyboard = $(keyboard_template, {class: "col"});
  this.container.height(key_dimensions.white.height);
  var white_index = 0;
  for(var i = 0; i < this.keys.length; i++){
    var key = this.keys[i];
    var key_string = null;
    if(key.color == "white") {
      key_string = Keys.White.render(white_index, key_dimensions, left_offset);
      white_index++;
    } else if (key.color == "black") {
      key_string = Keys.Black.render(white_index - 1, key_dimensions, left_offset);
    } else {
      alarm("Unbekannte Farbe");
    }
    if (key_string == null){
      alarm("Es wurde kein key_string erzeugt!")
    }
    var $element = $(key_string);
    var $delegate = $.proxy(this.keyPressed, this);
    $element.click(key, $delegate);
    $keyboard.append($element);
  }
  // Neue Ansicht in den DOM einhängen
  this.container.append($keyboard);
};

/**
 * Keyboard.keyPressed - Eine Taste wurde gedrückt - führe die registrierten Event-Handler aus.
 *
 * @param  {type} event_data Event-Daten
 */
Keyboard.prototype.keyPressed = function (event_data) {
  var note = event_data.data;
  var copyNote = {};
  Object.assign(copyNote, note)
  this.key_pressed_handlers.forEach(function(handler_record){
    var handler = handler_record.handler;
    if ("context" in handler_record){
      handler.call(handler_record.context, copyNote);
    }
    else {
      handler.call(copyNote);
    }
  });
};

/**
 * Keyboard.add_keypressed_handler - Füge einen neuen Handler für gedrückte Klaviertasten hinzu.
 *
 * @param  {type} handler        Funktion die ausgeführt werden soll
 * @param  {type} context = null Kontext in welchem die Funktion ausgeführt werden soll
 */
Keyboard.prototype.add_keypressed_handler = function (handler, context = null) {
  var handler_record = {handler: handler}
  if (context != null){
    handler_record.context = context;
  }
  this.key_pressed_handlers.push(handler_record);
};

/**
 * Objekt mit Funktionen etc für Klaviertasten
 */
var Keys = {
  SIGN: "b",  // Vorzeichen behaftete Note
  /**
   * Weiße Tasten
   */
  White: {
    MAX_WIDTH: 65,
    name: "white",
    /**
     * render - Erzeuge das Template der Taste mit den notwendigen Styles zur positionierung
     *
     * @param  {type} index           Index im Kontext des aktuellen renderings
     * @param  {type} spec            Berechnete Ausmaße
     * @param  {type} left_offset = 0 Linker Versatz
     * @return {type}                 HTML-String
     */
    render: function(index, spec, left_offset = 0) {
      var width = spec.white.width;
      var height = spec.white.height;
      var offset = index * width + left_offset;
      var template = `
        <div class="piano-key-white" style="top: 0px; left: ${offset}px; width: ${width}px; height: ${height}px"></div>
      `;
      return template;
    }
  },
  /**
   * Schwarze Tasten
   */
  Black: {
    name: "black",
    /**
     * render - Erzeuge den HTML-String zur Taste
     *
     * @param  {type} index           Index in der aktuellen Klaviertastatur
     * @param  {type} spec            Berechnete Ausmaße
     * @param  {type} left_offset = 0 Linker Versatz
     * @return {type}                 HTML-String
     */

    render: function(index, spec, left_offset = 0){
      var width = spec.black.width;
      var height = spec.black.height;
      var o_offset = spec.white.width - width / 2;
      var offset = o_offset + (index * spec.white.width) + left_offset;
      var template = `
        <div class="piano-key-black" style="top: 0px; left: ${offset}px; width: ${width}px; height: ${height}px"></div>
      `;
      return template;
    }
  },
  /**
   * CalcDimensions - Berechne die Tastenausmaße zur aktuellen Konfiguration/Viewportgröße
   *
   * @param  {type} container_width Container-Breite
   * @param  {type} white_keys      Anzahl weißer Tasten
   * @return {type}                 Objekt mit den berechneten Werten
   */

  CalcDimensions: function(container_width, white_keys) {
    var calculated_width = container_width / white_keys;
    if (calculated_width > Keys.White.MAX_WIDTH) {
      calculated_width = Keys.White.MAX_WIDTH;
    }
    var width_white = calculated_width;
    var height_white = width_white * 7;
    var width_black = width_white * 0.8;
    var height_black = height_white * (5/9);
    var result = {
      black: {
        height: Math.floor(height_black),
        width: Math.floor(width_black)
      },
      white: {
        height: Math.floor(height_white),
        width: Math.floor(width_white)
      }
    };
    return result;
  }
}

/**
 * Objekt für die Midi-Schnittstelle
 */
var Midi = {
  OCTAVE: 8,
  WHITE_KEYS: [0,2,4,5,7,9,11],
  NOTE_NAMES: ["C", "D", "E", "F", "G", "A", "B"],
  /**
   * Notes - Erzeuge die Midi-Informationen für eine gegebene Noten-Reichweite
   *
   * @param  {type} start Midi-Startnote
   * @param  {type} end   Midi-Endnote
   * @return {type}       Objekt mit den berechneten Midi-Informationen
   */
  Notes: function(start, end) {
    var notes = [];
    var current_key = null;
    start = parseInt(start)
    end = parseInt(end)
    for (var c_note = start; c_note <= end; c_note++){
      var o_note = c_note % 12;
      var octave = Math.floor((c_note - 12) / 12);
      var key_color = Keys.Black.name;
      var sign = "";
      if(Midi.WHITE_KEYS.includes(o_note)){
        key_color = Keys.White.name;
        current_key = Midi.NOTE_NAMES[Midi.WHITE_KEYS.indexOf(o_note)];
      }
      else {
        sign = Keys.SIGN;
      }
      if (current_key == null){
        alarm("The heck");
      }
      var note_name = current_key + sign + octave;
      var note = {color: key_color, midi: c_note, name: note_name, octave: octave, sign: sign};
      notes.push(note);
    }
    return notes;
  }
}

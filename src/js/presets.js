/**
 * Modul für die voreingestellten Ausgabeoptions-Templates.
 */

// Die Vorbelegten Formate
const PRESET_DEFAULTS = {
  "Listen": [
    {
      name: "Noten",
      includeName: true,
      seperateTextbox: false,
      namePattern: "# $name",
      delimiter: "custom",
      customDelimiter: ",",
      bounds: {
        left: `notes = [`,
        right: "]"
      },
      noteFormat: "midi",
      itemFormat: "$note"
    },
    {
      name: "Pausen",
      includeName: true,
      seperateTextbox: false,
      namePattern: "# $name",
      delimiter: "custom",
      customDelimiter: ",",
      bounds: {
        left: `pauses = [`,
        right: "]"
      },
      timeFormat: "ss",
      itemFormat: `$pause`
    }
  ],
  "Sonic-pi Play": [
    {
      name: "Sonic-Pi play",
      includeName: false,
      seperateTextbox: false,
      namePattern: "",
      delimiter: "newline",
      bounds: {
        left: "",
        right: ""
      },
      noteFormat: "midi",
      timeFormat: "ss",
      itemFormat: `play $note
sleep $pause`
    }
  ],
  "Recording": [
    {
      name: "Aufnahme",
      includeName: false,
      seperateTextbox: true,
      namePattern: "",
      delimiter: "custom",
      customDelimiter: ",",
      bounds: {
        left: "[",
        right: "]"
      },
      noteFormat: "midi",
      timeFormat: "sss",
      itemFormat: '{"midi": $note, "time": $time, "pause": $pause}'
    }
  ],
  "CSV": [
    {
      name: "CSV",
      includeName: true,
      seperateTextbox: false,
      namePattern: "Zeit;Note;Pause",
      delimiter: "newline",
      bounds: {
        left: "",
        right: ""
      },
      noteFormat: "midi",
      timeFormat: "sss",
      itemFormat: "$time;$note;$pause"
    }
  ]
}

/**
 * Template-Selector Objekt
 */
var PresetSelector = {
  /**
   * render - Erzeuge den HTML String und instanziiere, sowie initialisiere das DOM-Element für den Preset-Selector.
   *
   * @param  {type} preset_destination description
   * @param  {type} context            description
   * @return {type}                    description
   */

  render: function(preset_destination, context){
    var template = `
      <div class="container">
        <div class="row">
          <div class="col">
            <h4>Presets</h4>
          </div>
        </div>
        <div class="row">
          <div class="col" name="button-container">

          </div>
        </div>
      </div>
    `;
    var container = $(template);
    for(var preset_name in PRESET_DEFAULTS){
      var preset = PRESET_DEFAULTS[preset_name];
      var button_template = `<button class="btn preset-button" type="button">${preset_name}</button>`;
      var button = $(button_template);
      button.bind('click', {e_context: context, e_preset: preset}, function(eventdata){
        var i_context = eventdata.data.e_context;
        var i_preset = eventdata.data.e_preset;
        preset_destination.call(i_context, i_preset);
      });
      container.find("div[name=button-container]").append(button);
    }
    return container;
  }
}

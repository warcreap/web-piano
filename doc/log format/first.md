# Erste Überlegungen zur Ausgabesyntax

Was ich eigentlich ursprünglich wollte war eine vernünftige Ausgabe für Sonic-Pi.

## Notizen

#### Zeitformat

```
sss
```

#### Notenformat

- midi `${midi}`
- name `:${name}${sign}${octave}`

### Sequenz

Für jedes Listenitem

**Delimiter** benötigt

- newline
- ";"
- custom

#### Sonic-Pi Template

```
sleep ${time}
play ${note}
```

#### Recording Template

```
[${midi},${time}]
```

### Mehrere Listen

Lassen sich anlegen

#### Formatieren

Listenformat:
```
name = [${list}]
```

Elementformat:
```
${midi}
```

Trennzeichen für jede Liste einzeln

```html
<div class="row">
  <div class="input-group mb-2">
    <div class="input-group-prepend">
      <div class="input-group-text">Liste</div>
    </div>
    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Username">
  </div>
</div>
```

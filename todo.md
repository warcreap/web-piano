# ToDo - Tracking ausstehender Tätigkeiten

## ToDo (...)

- Resize der Seite bewirkt redraw des Keyboards // nicht trivial
- Design der Seite aufpeppen
  - Font-Awesome/Glyphs zum laufen bringen
- Hilfefunktion

### (7) Projektabschluss // Check

- Kommentare und Docstrings // Check
- Neuer Ausgabeparameter: $pause (n-1) // Check

### (6) Tastaturfeature // Check

- Klaviertasten mit Tastatur bedienbar machen
  - Controls implementieren
    - 1-2 Oktaven
    - Startoktave
    - bei 1 Oktave -> Startposition Tastatur
  - Handler
    - registrieren
    - löschen
- Position der Optionen bei Klavieroptionen

### (5) Playbackfeature

- Playback
  - Datei upload // Check
  - Wechsle Architektur // Check
  - Text-Highlighting // Check
  - Stop // Check
  - Clear // Check
- Recording Format klären
  - Zeitformate // Check
    - Sekunden := `1.123`
    - Millisekunden := `1123`

### (4) Finalisiere Ausgabefeature

- Ausgabe
  - Copy to Clipboard // Check
  - Speichern // Check
  - Markierter Text nicht sichtbar // Check
  - Nur erstes separates Textlog wird aktualisiert // Check

### (3) Pianotastatur fertig stellen

- Volle Tasten (O1/-2:O7) // Check
  - Midi Informationen vervollständigen // Check
- Audio für ALLE Tasten in geringer Größe // Check
- Custom Output Format // Check

### (2) TextLog Page Optimierung

- Handler erstellen
- Aufbau der Seite und Templating optimieren

### (1) Pianotastatur soll bedienbar sein.

In der ersten Iteration soll ein funktionierender Prototyp geschaffen werden,
welcher nur aus einer bedienbaren Klaviertastatur besteht.

__Es werden benötigt__:
- Ein Konzept zur Erstellung und sinnvollen Gliederung der Web-Oberfläche
  - Bootstrap?
  - Templates?
  - jQuery?
- Eine Möglichkeit eine bedienbare Klaviertastatur im Browser abzubilden
- Die Klänge der verschiedenen auf einem Klavier spielbaren Noten
  - In welcher Form, wenn diese später noch bearbeitet werden sollen?

#### Tl;dr
- Klaviersounds
- Oberflächenentwurf
- Umsetzung des Entwurfs

### jQuery

!!! OFFLINE DOKU !!!

- DOM Listener, wenn ready,
- Elemente hinzufügen
- Klasse setzen, position anpassen
- Click-listener für playSound
- Elemente aus DOM löschen (wenn neue Einstellungen für Oktaven geladen)
